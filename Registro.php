<?php
$message = "";
if (!empty($_REQUEST['status'])) {
  switch ($_REQUEST['status']) {
    case 'login':
      $message = 'User does not exists';
      break;
    case 'error':
      $message = 'There was a problem inserting the user';
      break;
  }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <title>Document</title>
</head>

<body>
    <div class="container">
        <div class="msg">
            <?php echo $message; ?>
        </div>


        <div class="card-body" style="background-color: lightblue;">
            <h1>User Register</h1>
            <hr class="my-4">
            <form action="saveUsers.php" method="POST" class="form-inline" role="form">
                <div class="form-group ">
                    <label class="sr-only" for="">Fisrtname</label>
                    <input type="text" class="form-control" id='' name="firstname" placeholder="FirtsName" required="Required">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="">Lastname</label>
                    <input type="text" class="form-control" id='' name="lastname" placeholder="Lastname" required="Required">
                </div>
                <br><br>
                <div class="form-group">
                    <label class="sr-only" for="">Email Address</label>
                    <input type="text" class="form-control" id='' name="email" placeholder="Email Address" required="Required">
                </div>
                
                <div class="form-group">
                    <label class="sr-only" for="">username</label>
                    <input type="text" class="form-control" id='' name="username" placeholder="Username" required="Required">
                </div>
                <br><br>
                <div class="form-group">
                    <label class="sr-only" for="">Password</label>
                    <input type="password" class="form-control" id='' name="password" placeholder="Your password" required="Required">
                </div>
                
                <button  style="align-items: center;" type="submit" class="btn btn-primary" a>SignUp</button>
        </div>



        </form>
    </div>

</body>

</html>