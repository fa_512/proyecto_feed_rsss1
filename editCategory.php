<?php
session_start();
$_SESSION['message'] = 'Category was create Successfully';
$_SESSION['message_type'] = 'succes';
$user = $_SESSION['user'];
if (!$user) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<?php require('funtions.php'); // get info by id request
$sources = getCategory2($_REQUEST);
foreach ($sources as $source)
    var_dump($source);
?>

<body>
    <?php if ($user['role'] === 'admin') { ?>
        <div class="container-fluid">
            <div class="jumbotron">
                <h1 class="display-4">Edit Categories</h1>
                <hr class="my-4">
            </div>
            <form method="POST" action="updateCategory.php" value="<?php echo $_REQUEST['id_category'] ?>">
                <div class="form-group">
                    <label for="name">Name_category</label>

                    <input id="name" value="<?php echo $source['name_category'] ?>" class="form-control" type="text" name="name_category">
                    <input id="id_category" value="<?php echo $source['id_category'] ?>" class="form-control" type="hidden" name="id_category">


                </div>



                <button type="submit" name="update" class="btn btn-info"> Update </button>

            </form>
        </div>
    <?php } else {

        header('Location: dashboard.php'); ?>
    <?php } ?>
</body>

</html>