<?php
session_start();

$user = $_SESSION['user'];
if (!$user) {
    header('Location: index.php');
}

?>


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<div class="container">
    <div class="row">
        <div class="col">
            <h1 class="text-light bg-dark"> My newsCover </h1>
        </div>

        <br>

        <div class="col">
            <a type="button" class="btn btn-info" href="dashboard.php"><?php echo $user['firstname'] ?></a>
            <a type="button" class="btn btn-danger" href="logout.php">Logout</a>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm">
            <nav class="nav">
                <?php if ($user['role'] === 'admin') { ?>
                    <!--validacion de ususario admin -->
                    <li class="nav-item">
                        <a type="button" class="btn btn-outline-info" href="categorias.php">Create Categories</a>
                    </li>

                <?php } ?>

        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <nav class="nav">
                        <?php if ($user['role'] !== 'admin') { ?>
                            <li class="nav-item">
                                <a type="button" class="btn btn-outline-warning" href="newSources.php">New Sources</a>
                            </li>
                        <?php } ?>
                </div>
                <br><br>

                <?php require('funtions.php');
                //$news1 = getnews();
                //$categ2 = getEspecifyCategory();
                $urls = getSources();
                foreach ($urls as $url)
                ?>
                <div class="container">
                    <div class="row">
                        <div class="col-sm">
                            <nav class="nav">

                                <li class="nav-item">
                                    <a type="button" class="btn btn-outline-info" href="categorias.php">Portada</a>
                                </li>
                                <li class="nav-item">
                                    <a type="button" value="<?php echo $news['name_category'] ?>" class="btn btn-outline-info" href="getNewsCategory.php">Sucesos</a>
                                </li>
                                <li class="nav-item">
                                    <a type="button" value="<?php echo $news['deportes'] ?>" class="btn btn-outline-info" href="categorias.php">Deportes</a>
                                </li>
                                <li class="nav-item">
                                    <a type="button" value="<?php echo $news['nacionales'] ?>" class="btn btn-outline-info" href="list.php">Nacionales</a>
                                </li>
                                <li class="nav-item">
                                    <a type="button" value="<?php echo $news['internacionales'] ?>" class="btn btn-outline-info" href="categorias.php">Internacionales</a>
                                </li>
                                <li class="nav-item">
                                    <a type="button" value="<?php echo $news['espectaculos'] ?>" class="btn btn-outline-info" href="list.php">Espectaculos</a>
                                </li>
                        </div>

                        <div>

                            <?php

                            $i = 0;
                           
                            $url = $url['url'];
                            $rss = simplexml_load_file($url);
                            foreach ($rss->channel->item as $item) {
                                $title = $item->title;  //extrae el titulo 
                                $link = $item->link;  //extrae el link
                                $date = $item->pubDate;  //extrae la fecha
                                $guid = $item->guid;  //extrae el link de la imagen
                                $description = strip_tags($item->description);  //extrae la descripcion
                                if (strlen($description) > 400) { //limita la descripcion a 400 caracteres
                                    $stringCut = substr($description, 0, 200);
                                    $description = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
                                }
                                if ($i < 16) { // extrae solo 16 items
                                    echo '<div class="cuadros1"><h4><a href="' . $link . '" target="_blank">' . $title . '</a></h4><br><img src="' . $guid . '"><br>' . $description . '<br><div class="time">' . $date . '</div></div>';
                                }
                                $i++;
                            }
                            echo '<div style="clear: both;"></div>';

                            ?>

                        </div>
                        <br><br>

                    </div>



                </div>