<?php
session_start();

$user = $_SESSION['user'];
if (!$user) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<?php require('funtions.php') ?>

<body>
    <?php if ($user['role'] === 'admin') { ?>
        <div class="container-fluid">
            <div class="jumbotron">
                <h1 class="display-4">Create Categories</h1>
                <p class="lead">This is the create process</p>
                <hr class="my-4">
            </div>
            <form method="post" action="saveCategory.php">
                <div class="form-group">
                    <label for="name">Name_category</label>
                    <input id="name" class="form-control" type="text" name="name">
                </div>

        </div>

        <button type="submit" class="btn btn-primary"> create </button>

        </form>
        </div>
        <div class="container">
                <h1>Your categories</h1>
                <table class="table table-light">
                    <tr>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    <tbody>
                        <?php

                        $categories = getCategory();


                        // loop categories
                        foreach ($categories as $cat) {
                            echo "<tr><td>" . $cat['name_category'] . "</td>
                                        <td><a href='editCategory.php?id_category=" .$cat['id_category']. "'>Edit</a> <a href='deleteCategory.php?id_category=". $cat['id_category'] . "'>delete</a></td></tr>";
                        }
                        $_SESSION['message'] = 'Category was create Successfully';
                        $_SESSION['message_type'] = 'succes';
                        ?>
                    </tbody>
                </table>
                <?php
                ?>
            </div>
    <?php } else {

        header('Location: categorias.php'); ?>
    <?php } ?>
</body>

</html>