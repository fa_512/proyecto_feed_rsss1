<?php
session_start();

$user = $_SESSION['user'];
if (!$user) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<?php require('funtions.php');
$sours = getSources();
?>

<body>
    <?php if ($user['role'] !== 'admin') { ?>
        <div class="container-fluid">
            <div class="jumbotron">
                <h1 class="display-4">New Sources</h1>
                <p class="lead">This is the Register process</p>
                <hr class="my-4">
            </div>
            <form method="post" action="saveSources.php">
                <div class="form-group">
                    <label for="last-name">RSS URL</label>
                    <input id="last-name" class="form-control" type="text" name="RSS">
                </div>
                <div class="form-group">
                    <label for="name"> Name</label>
                    <input id="name" class="form-control" type="text" name="name">
                </div>

                <div class="form-group">
                    <label for="carreras">Categories</label>
                    <select id="carreras" class="form-control" name="id_category">
                        <?php
                        $categorys = getCategory();
                        foreach ($categorys as $category) { ?>

                            <option value="<?php echo $category['id_category'] ?>"><?php echo $category['name_category'] ?></option>;//imprime carreras
                        <?php } ?>
                        < </select>
                </div>
                <div class="form-group">
                    <input id="user_id" value="<?php echo $user['id_user'] ?>" class="form-control" type="hidden" name="user_id">



                    <button type="submit" href="newSources.php" class="btn btn-primary"> Register </button>

            </form>
            <!-- Table show the news cover-->
            <div class="container">
                <h1>Your unique news cover</h1>
                <table class="table table-light">
                    <tr>
                        <th>Name</th>
                        <th>Category</th>
                    </tr>
                    <tbody>


                        <?php
                        // loop users
                        foreach ($sours as $source) {
                            echo "<tr><td>" . $source['name'] . "</td><td>" . $source['name_category'] . "</td><td><a href='editSources.php?id=" . $source['id_sources'] . "'>Edit</a>
                             <a href='deleteSources.php?id=" . $source['id_sources'] . "'>delete</a></td></tr>";
                        }

                        ?>
                    </tbody>
                </table>
                <?php
                ?>
            </div>
        </div>
    <?php } else {
        header('Location: newSources.php'); ?>
    <?php } ?>
</body>

</html>