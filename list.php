<?php
session_start();

$user = $_SESSION['user'];
if (!$user) {
    header('Location: index.php');
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Document</title>
</head>

<body>
    <?php if ($user['role'] === 'admin') { ?>
        <div class="container">
            <?php require('funtions.php')
            //$lists2 = getCarrers();
            ?>
            <h1>Categories Lists</h1>
            <table class="table table-striped">
                <tr>
                    <th>name_category</th>


                </tr>
                <tbody>
                    <?php
                    // loop users
                    $catgs = getCategory();
                    foreach ($catgs as $catg) {

                        echo "<tr><td>" . $catg['name'] . "</td></tr>";
                    }
                    ?>
                </tbody>
            </table>

        </div>
    <?php } ?>

</body>

</html>