<?php
session_start();

$user = $_SESSION['user'];
if (!$user) {
    header('Location: index.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<?php require('funtions.php');// get info by id request
$sources = getSources2($_REQUEST);
foreach ($sources as $source)
?>

<body>
    <?php if ($user['role'] !== 'admin') { ?>
        <div class="container-fluid">
            <div class="jumbotron">
                <h1 class="display-4">New Sources</h1>
                <p class="lead">This is the Register process</p>
                <hr class="my-4">
            </div>
            <form method="POST" action="updateSources.php" value ="<?php echo $_REQUEST['id']?>">
                <div class="form-group">
                    <label for="last-name">RSS URL</label>
                    <input id="url" value="<?php echo $source['url'] ?>" class="form-control" type="text" name="RSS">
                </div>
                <div class="form-group">
                    <label for="name"> Name</label>
                    <input id="name" value="<?php echo $source['name'] ?>" class="form-control" type="text" name="name">
                </div>

                <div class="form-group">
                    <label for="categories">Categories</label>
                    <select id="categories" class="form-control" name="id_category">
                        <?php
                        $categorys = getCategory();
                        foreach ($categorys as $category) { ?>
                            <option value="<?php echo $category['id_category'] ?>"><?php echo $category['name_category'] ?></option>;//imprime carreras
                        <?php } ?>
                        < </select>
                </div>
                <div class="form-group">
                    <input id="user_id" value="<?php echo $source['user_id'] ?>" class="form-control" type="hidden" name="user_id">
                    <input id="id_sources" value="<?php echo $source['id_sources'] ?>" class="form-control" type="hidden" name="id_sources">

                </div>
                <div class="form-group">
                    <input id="user_id" value="<?php echo $source['user_id'] ?>" class="form-control" type="hidden" name="user_id">
                    <input id="id_sources" value="<?php echo $source['id_sources'] ?>" class="form-control" type="hidden" name="id_sources">

                </div>
                <div>
                    <button type='submit' name="update" class="btn btn-info"> Update </button>
                </div>

            </form>

        </div>
    <?php } else {

        header('Location: dashboard.php'); ?>
    <?php } ?>
</body>

</html>