<?php

/**
 *  Gets a new mysql connection
 */
function getConnection()
{
  $connection = new mysqli('localhost', 'root', '', 'proyecto1');
  if ($connection->connect_errno) {
    printf("Connect failed: %s\n", $connection->connect_error);
    die;
  }

  return $connection;
}





/**
 * Get one specific user from the database
 *
 */
function authenticate($username, $password)
{
  $conn = getConnection();
  $sql = "SELECT * FROM `users` WHERE `username` =  '$username' AND `password` = '$password'";
  $result = $conn->query($sql);


  if ($conn->connect_errno) {
    $conn->close();
    return false;
  }
  $conn->close();
  return $result->fetch_array();
}
/**
 * save info on the database
 *
 */
function saveUser($user)
{
  $name = $user['firstname'];
  $lastName = $user['lastname'];
  $email = $user['email'];
  $username = $user['username'];
  $password = $user['password'];

  $conn = getConnection();
  $sql = "INSERT INTO users ( `firstname`,`lastname`, `email`, `username`, `password`) VALUES ('$name', '$lastName','$email',  '$username', '$password')";
  mysqli_query($conn, $sql);
}

function saveCategory($category)
{
  $name = $category['name'];
  $conn = getConnection();
  $sql = "INSERT INTO categories ( `name_category`) VALUES ('$name')";
  mysqli_query($conn, $sql);
}

function  saveSources($source)
{

  $url = $source['RSS'];
  $name = $source['name'];

  $category = $source['id_category'];
  $user = $source['user_id'];
  $conn = getConnection();
  $sql = "INSERT INTO newssources ( `url`, `name`, `category_id`, `user_id`) VALUES ('$url','$name', '$category', '$user')";
  mysqli_query($conn, $sql);
}

/**
 *  Gets the info from the database
 */
function getCategory()
{
  $conn = getConnection();
  $sql = "SELECT * FROM `categories`";
  $result = $conn->query($sql);

  return $result;
}

function getEspecifyCategory()
{
  $conn = getConnection();
  $sql = "SELECT * FROM `categories`";
  $result = $conn->query($sql);

  return $result;
}

function getUser()
{
  $conn = getConnection();
  $sql = "SELECT * FROM `user`";
  $result = $conn->query($sql);

  return $result;
}

function getSources()
{
  $conn = getConnection();
  $sql = "SELECT * FROM `newssources` INNER JOIN categories ON category_id = id_category";
  $result = $conn->query($sql);


  return $result;
}

function getSources2($source)
{
  $id = $source['id'];
  $conn = getConnection();
  $sql = "SELECT * FROM `newssources` where id_sources = '$id'";
  $result = $conn->query($sql);


  return $result;
}

function getCategory2($source)
{
  $id = $source['id_category'];
  $conn = getConnection();
  $sql = "SELECT * FROM `categories` where id_category = '$id'";
  $result = $conn->query($sql);


  return $result;
}



function getnews()
{
  $conn = getConnection();
  $sql = "SELECT news.title, news.short_description, categories.name_category FROM `news` INNER JOIN categories ON news.category_id = id_category ";
  $result = $conn->query($sql);

  return $result;
}

/**
 *  modificate the info on the database
 */
function updateSources($source)
{
  $id = $source['id_sources'];
  $url = $source['RSS'];
  $name = $source['name'];
  $category = $source['id_category'];
  $user = $source['user_id'];
  $conn = getConnection();
  $sql = "UPDATE `newssources` SET `url`='$url',`name`='$name',`category_id`='$category',`user_id`='$user' WHERE `id_sources`= '$id'";
  mysqli_query($conn, $sql);
}

function deleteSources($source)
{
  $id = $source['id_sources'];
  $conn = getConnection();
  $sql = "DELETE FROM `newssources` WHERE `id_sources`= '$id'";
  mysqli_query($conn, $sql);
}


function updateCategory($source)
{
  $id = $source['id_category'];
  $name = $source['name_category'];
  $conn = getConnection();
  $sql = "UPDATE `categories` SET `name_category`='$name' WHERE `id_category`= '$id'";
  mysqli_query($conn, $sql);
}

function deleteCategory($source)
{
  $id = $source['id_category'];
  $conn = getConnection();
  $sql = "DELETE FROM `categories` WHERE `id_category`= '$id'";
  mysqli_query($conn, $sql);
}
