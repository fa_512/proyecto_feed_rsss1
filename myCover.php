<?php
session_start();

$user = $_SESSION['user'];
if (!$user) {
  header('Location: index.php');
}

?>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Register</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<div class="container">
  <div class="row">
    <div class="col">
      <h1 style="background-color: turquoise;"> Bienvenido <?php echo $user['name'];
                      echo " ";
                      echo $user['lastname'];
                      echo " <br>";
                      echo "Roll: "; echo $user['roll'] ?> </h1>
    </div>

    <br>
    <div class="col">
      <a type="button" class="btn btn-danger" href="logout.php">Logout</a>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm">
      <nav class="nav">
        <?php if ($user['roll'] !== 'admin') { ?>
          <li class="nav-item">
            <a type="button" class="btn btn-outline-info" href="matricula.php">Add new Feed</a>
          </li>
          <li class="nav-item">
            <a type="button" class="btn btn-outline-info" href="list.php">Edit Categories</a>
          </li>
        <?php } ?>
    </div>